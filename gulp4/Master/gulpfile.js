var gulp = require('gulp')
var runSequence = require('run-sequence')
var bowerInjection = require('./Modules/Pages/Scripts/Variables/FlexoCMS.bowerInjection.js')
try {
  var paths = require('../../../../Gulp/Options/Paths.js')
  var sass = require('../../../../Gulp/Tasks/Css/Sass.js')
  var typescript = require('../../../../Gulp/Tasks/Typescript/Typescript.js')
  var bowerInstall = require('../../../../Gulp/Tasks/Bower/BowerInstall.js')
  var bowerConcat = require('../../../../Gulp/Tasks/Bower/BowerConcat.js')
  var linter = require('../../../../Gulp/Tasks/Lint/Linter.js')
  var duplicate = require('../../../../Gulp/Tasks/Duplicate/duplicate.js')
  var register = require('../../../../Gulp/Tasks/Register/RegisterModules.js')
  var createTs = require('../../../../Gulp/Tasks/CreateTemplate/CreateTs.js')
  var pathToCreate = '../../../../Gulp/Tasks/CreateTemplate'
} catch (error) {
  var paths = require('../../../../../../Gulp/Options/Paths.js')
  var sass = require('../../../../../../Gulp/Tasks/Css/Sass.js')
  var typescript = require('../../../../../../Gulp/Tasks/Typescript/Typescript.js')
  var bowerInstall = require('../../../../../../Gulp/Tasks/Bower/BowerInstall.js')
  var bowerConcat = require('../../../../../../Gulp/Tasks/Bower/BowerConcat.js')
  var linter = require('../../../../../../Gulp/Tasks/Lint/Linter.js')
  var duplicate = require('../../../../../../Gulp/Tasks/Duplicate/duplicate.js')
  var register = require('../../../../../../Gulp/Tasks/Register/RegisterModules.js')
  var createTs = require('../../../../../../Gulp/Tasks/CreateTemplate/CreateTs.js')
  var pathToCreate = '../../../../../../Gulp/Tasks/CreateTemplate'
}

// Bower
gulp.task('bowerInstall', bowerInstall(paths.front))
gulp.task('bowerConcat', bowerConcat([paths.front.shared, paths.front.modules, paths.master.shared, paths.master.modules], bowerInjection.bowerInjections, paths.front.bower.bowerDirectory, paths.front.dist))

gulp.task('bower', gulp.series('bowerInstall', 'bowerConcat'))


// Sass
gulp.task('sass', sass(paths.front.css.compilerSass, paths.front.dist, 'FlexoCMS.Theme'))

// Typescript

gulp.task('ts:Master', typescript(paths.front.ts.compilerTsTheme, paths.front.dist, 'FlexoCMS.Master'))

// Plugins
gulp.task('duplicate:Plugins', duplicate(paths.front.modules, 'Scripts/Plugins', paths.front.dist + '/Plugins'))

// Register Modules
gulp.task('register:Modules', register(paths.front.modules, paths.front.css.stylesFolder, '_modules.scss', 'scss', false, '../../'))

// Init

gulp.task('init', gulp.series('bower', 'duplicate:Plugins','register:Modules', gulp.parallel('sass', 'ts:Master')))

// Watch
gulp.task('watch:sass', function() {
  gulp.watch(paths.front.css.watchSass, gulp.parallel('sass'))
})

gulp.task('watch:ts:Master', function() {
  gulp.watch(paths.front.ts.watchCompilerTsTheme, gulp.parallel('ts:Master'))
})

gulp.task('watch', gulp.parallel('watch:sass', 'watch:ts:Master'))

// Lint
gulp.task('lint:Master', linter(paths.front.ts.compilerTsMaster))

gulp.task('create:ts', createTs(paths.front.modules, pathToCreate))