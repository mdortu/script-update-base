@echo off

if exist ..\flexocms (
  if exist ..\flexocms\Gulp\ (
    ECHO Copy Gulp folder from Flexo
    if exist .\Gulp (
      del .\Gulp /S /F /Q > NUL
      rmdir /Q/S Gulp
    )
    xCopy ..\flexocms\Gulp\ .\Gulp /E /H /C /I
  )
  if exist ..\flexocms\package.json (
    ECHO Copy package.json from Flexo 
    copy ..\flexocms\package.json .\
  )
  if exist ..\flexocms\FlexoCMS.Web.UI\NEW\Website\Instances\Admin\gulpfile.js (
    ECHO Copy gulpfile Admin
    copy ..\flexocms\FlexoCMS.Web.UI\NEW\Website\Instances\Admin\gulpfile.js .\Website\Instances\Admin
  )
  if exist ..\flexocms\FlexoCMS.Web.UI\NEW\Website\Instances\Admin\Content\Styles\typography.less (
    ECHO Replace typography.less admin
    if exist .\Website\Instances\Admin\Content\Styles\typography.less (
      del .\Website\Instances\Admin\Content\Styles\typography.less /F /Q > NUL
    )
    copy ..\flexocms\FlexoCMS.Web.UI\NEW\Website\Instances\Admin\Content\Styles\typography.less .\Website\Instances\Admin\Content\Styles\typography.less
  )

  if exist .\Website\Instances\Front\Master\gulpfile.js (
    ECHO replace gulpfile master
    del .\Website\Instances\Front\Master\gulpfile.js /S /F /Q > NUL
    curl.exe --output  .\Website\Instances\Front\Master\gulpfile.js --url https://gitlab.com/mdortu/script-update-base/-/raw/master/gulp4/Master/gulpfile.js
  )

  if exist .\Website\Instances\Front\Shop\gulpfile.js (
    ECHO replace gulpfile shop
    del .\Website\Instances\Front\Shop\gulpfile.js /S /F /Q > NUL
    curl.exe --output  .\Website\Instances\Front\Shop\gulpfile.js --url https://gitlab.com/mdortu/script-update-base/-/raw/master/gulp4/Shop/gulpfile.js
  )

  if exist .\Website\Instances\Front\Corporate\gulpfile.js (
    ECHO replace gulpfile Corporate
    del .\Website\Instances\Front\Corporate\gulpfile.js /S /F /Q > NUL
    curl.exe --output  .\Website\Instances\Front\Corporate\gulpfile.js --url https://gitlab.com/mdortu/script-update-base/-/raw/master/gulp4/Shop/gulpfile.js
  )
)

if exist .\node_modules (
  ECHO Deleting node_modules folder...
  del .\node_modules /S /F /Q > NUL
  rmdir /Q/S node_modules
  ECHO node_modules folder deleted!
)

if exist .\package-lock.json (
  ECHO Deleting package lock file...
  del .\package-lock.json
  ECHO Lock file deleted
)

if exist .\npm-shrinkwrap.json (
  ECHO Deleting npm-shrinkwrap.json file...
  del .\npm-shrinkwrap.json
  ECHO npm-shrinkwrap.json deleted
)

ECHO rebuilding node_modules...
npm i
