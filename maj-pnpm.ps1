# =======================================================
# NAME: XXXXXX.ps1
# AUTHOR: Synchrone
# DATE: 11/2020
#
# COMMENTS: Desription des traitements
# =======================================================

# =======================================================
# Méthodes utiles
# =======================================================

function GetDirectories([string] $path){
    return Get-ChildItem $path -Directory
}

function GetCurrentPath(){
    return Get-Location
}

function CheckIfExist([string] $path){
    return Test-Path $path
}

function UpdateFolder([string] $from, [string] $to, $exclude = "") {
    if(CheckIfExist $from){
        Write-Output "Updating $from"
        Copy-Item -Path $from -Recurse -Destination $to -Container 
    }
}

function CreateFolder([string] $path){
    New-Item -Path $path -ItemType Directory
}

function Delete([string] $path){
   if(CheckIfExist $path){
        Remove-Item -Path $path -Recurse -Force
    }
}

function CreateFile([string] $path){
    New-Item -Path $path -ItemType File -force
}

function DeleteAndUpdate([string] $from, [string] $to, $filter = ""){
    if(CheckIfExist $to){
        Delete $to
    }

    UpdateFolder $from $to $filter
}

function DeleteAndUpdateWithFilter($from, $to, $exclude) {
    Delete $to
    CopyFolderWithFilter $from $to $exclude
}

function CopyFolderWithFilter($from, $to, $exclude){
    Get-ChildItem -Path $from -Directory -Recurse -Exclude $exclude | Where-Object { $_.FullName -notmatch $exclude} | Select-Object FullName, @{N="NewPath";E={$_.FullName.Replace($from, $to)}} | ForEach-Object { New-Item -Path $_.NewPath -ItemType "Directory" -Force }
    Get-ChildItem -Path $from -File -Exclude $exclude -Recurse | Where-Object { $_.FullName -notmatch $exclude}| Select-Object FullName, @{N="NewPath";E={$_.FullName.Replace($from, $to)}} | ForEach-Object { Copy-Item -Path $_.FullName -Destination $_.NewPath -Force }
}

# =======================================================
# Path d'environnement
# =======================================================

$basePath = Get-Location

$frontFolder = "$basePath\Website\Instances\Front"
$adminFolder = "$basePath\Website\Instances\Admin"

$frontModules = GetDirectories "$frontFolder\Master\Modules"
$adminModules = GetDirectories "$adminFolder\Modules"


$flexoBaseFolder = Read-Host -Prompt 'Input your flexoCMS path (enter empty : C:\Websites\flexocms)'

if(!$flexoBaseFolder){
    $flexoBaseFolder = "C:\Websites\flexocms"
}

Write-Output($flexoBaseFolder)

$flexoAdmin = "$flexoBaseFolder\FlexoCMS.Web.UI\NEW\Website\Instances\Admin";
$flexoFront = "$flexoBaseFolder\FlexoCMS.Web.UI\NEW\Website\Instances\Front";

# =======================================================
# Logique du script ci-dessous
# =======================================================


# =======================================================
# MAJ Front
# =======================================================

Write-Output "Update Files and folder ..."

DeleteAndUpdate "$flexoBaseFolder/Gulp" "$basePath/Gulp"
DeleteAndUpdate "$flexoBaseFolder/package.json" "$basePath/package.json"
DeleteAndUpdate "$flexoAdmin/Modules/Catalog.Admin/Content/Scripts/Src/Components/ProductPrices.jsx" "$adminFolder/Modules/Catalog.Admin/Content/Scripts/Src/Components/ProductPrices.jsx"

Write-Output "Deleting node modules ..."
Delete "$basePath/node_modules"

Write-Output "Deleting package.lock ..."
Delete "$basePath/package.lock.json"

Write-Output "Deleting npm-shrinkwrap.json ..."
Delete "$basePath/npm-shrinkwrap.json"

Write-Output "Deleting pnpm-lock.yaml ..."
Delete "$basePath/pnpm-lock.yaml"

$compileAfterUpdate = ""

while ($compileAfterUpdate -notmatch "y|n")
{
    $compileAfterUpdate = Read-Host -Prompt 'Do you want to compile admin ? (Y/N)'
    if($compileAfterUpdate.ToLower() -eq 'n')
    {
        Exit
    }
}

cd "$basePath"

Write-Output "Compiling Admin ..."

cd "$adminFolder"
cmd.exe /c pnpm i
cmd.exe /c gulp init

Write-Output "Admin compiled"
