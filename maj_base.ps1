# =======================================================
# NAME: XXXXXX.ps1
# AUTHOR: Synchrone
# DATE: 11/2020
#
# COMMENTS: Desription des traitements
# =======================================================

# =======================================================
# Méthodes utiles
# =======================================================

function GetDirectories([string] $path){
    return Get-ChildItem $path -Directory
}

function GetCurrentPath(){
    return Get-Location
}

function CheckIfExist([string] $path){
    return Test-Path $path
}

function UpdateFolder([string] $from, [string] $to, $exclude = "") {
    if(CheckIfExist $from){
        Write-Output "Updating $from"
        Copy-Item -Path $from -Recurse -Destination $to -Container 
    }
}

function CreateFolder([string] $path){
    New-Item -Path $path -ItemType Directory
}

function Delete([string] $path){
   if(CheckIfExist $path){
        Remove-Item -Path $path -Recurse -Force
    }
}

function CreateFile([string] $path){
    New-Item -Path $path -ItemType File -force
}

function DeleteAndUpdate([string] $from, [string] $to, $filter = ""){
    Delete $to
    UpdateFolder $from $to $filter
}

function DeleteAndUpdateWithFilter($from, $to, $exclude) {
    Delete $to
    CopyFolderWithFilter $from $to $exclude
}

function CopyFolderWithFilter($from, $to, $exclude){
    Get-ChildItem -Path $from -Directory -Recurse -Exclude $exclude | Where-Object { $_.FullName -notmatch $exclude} | Select-Object FullName, @{N="NewPath";E={$_.FullName.Replace($from, $to)}} | ForEach-Object { New-Item -Path $_.NewPath -ItemType "Directory" -Force }
    Get-ChildItem -Path $from -File -Exclude $exclude -Recurse | Where-Object { $_.FullName -notmatch $exclude}| Select-Object FullName, @{N="NewPath";E={$_.FullName.Replace($from, $to)}} | ForEach-Object { Copy-Item -Path $_.FullName -Destination $_.NewPath -Force }
}

# =======================================================
# Path d'environnement
# =======================================================

$basePath = Get-Location

$frontFolder = "$basePath\Website\Instances\Front"
$adminFolder = "$basePath\Website\Instances\Admin"

$frontModules = GetDirectories "$frontFolder\Master\Modules"
$adminModules = GetDirectories "$adminFolder\Modules"


$flexoBaseFolder = Read-Host -Prompt 'Input your flexoCMS path (enter empty : C:\Websites\flexocms)'

if(!$flexoBaseFolder){
    $flexoBaseFolder = "C:\Websites\flexocms"
}

Write-Output($flexoBaseFolder)

$flexoAdmin = "$flexoBaseFolder\FlexoCMS.Web.UI\NEW\Website\Instances\Admin";
$flexoFront = "$flexoBaseFolder\FlexoCMS.Web.UI\NEW\Website\Instances\Front";

# =======================================================
# Logique du script ci-dessous
# =======================================================


# =======================================================
# MAJ Front
# =======================================================

Write-Output "Update Files and folder from Front ..."

DeleteAndUpdate "$flexoFront/Config" "$frontFolder/Config"
DeleteAndUpdate "$flexoFront/Webpack" "$frontFolder/Webpack"
DeleteAndUpdate "$flexoFront/typings" "$frontFolder/typings"
DeleteAndUpdate "$flexoFront/.babelrc" "$frontFolder/.babelrc"
DeleteAndUpdate "$flexoFront/.prettierrc" "$frontFolder/.prettierrc"
DeleteAndUpdate "$flexoFront/serverConfig.js" "$frontFolder/serverConfig.js"
DeleteAndUpdate "$flexoFront/tsconfig.json" "$frontFolder/tsconfig.json"
DeleteAndUpdate "$flexoFront/.eslintrc.js" "$frontFolder/.eslintrc.js"
DeleteAndUpdate "$flexoFront/.eslintignore" "$frontFolder/.eslintignore"
DeleteAndUpdate "$flexoFront/typings.json" "$frontFolder/typings.json"
DeleteAndUpdate "$flexoFront/webpack.config.js" "$frontFolder/webpack.config.js"
DeleteAndUpdate "$flexoFront/Master/Images" "$frontFolder/Master/Images"
DeleteAndUpdate "$flexoFront/Master/Shared" "$frontFolder/Master/Shared"
DeleteAndUpdate "$flexoFront/Master/package.json" "$frontFolder/Master/package.json"
DeleteAndUpdate "$flexoFront/package.json" "$frontFolder/package.json"
Delete "$basePath/Website/.eslintrc.json"
Delete "$frontFolder/tslint.json"
Delete "$frontFolder/yarn.lock"
Delete "$frontFolder/node_modules"
Delete "$frontFolder/bower_components"
Delete "$frontFolder/Content"
Delete "$frontFolder/Corporate/gulpfile.js"
Delete "$frontFolder/Corporate/Dist"
Delete "$frontFolder/Shop/gulpfile.js"
Delete "$frontFolder/Shop/Dist"

foreach ($module in $frontModules){
  DeleteAndUpdate "$flexoFront/Master/Modules/$module" "$frontFolder/Master/Modules/$module"
}


# =======================================================
# MAJ Admin
# =======================================================

Write-Output "Update Files and folder from Admin ..."

DeleteAndUpdateWithFilter "$flexoAdmin\Content" "$adminFolder\Content" "Bower_components|Dist"
DeleteAndUpdate "$flexoAdmin/Shared" "$adminFolder/Shared"
DeleteAndUpdate "$flexoAdmin/gulpfile.js" "$adminFolder/gulpfile.js"
DeleteAndUpdate "$flexoBaseFolder/package.json" "$basePath/package.json"
DeleteAndUpdate "$flexoBaseFolder/Gulp" "$basePath/Gulp"
foreach ($module in $adminModules){
   DeleteAndUpdate "$flexoAdmin/Modules/$module" "$frontAdmin/Modules/$module"
}

# =======================================================
# MAJ NPM
# =======================================================

Write-Output "Deleting node modules ..."
Delete "$basePath/node_modules"

Write-Output "Deleting package.lock ..."
Delete "$basePath/package.lock.json"

Write-Output "Deleting pnpm ..."
Delete "$basePath/pnpm-lock.yaml"

Write-Output "Deleting npm-shrinkwrap.json ..."
Delete "$basePath/npm-shrinkwrap.json"

$compileAfterUpdate = ""

while ($compileAfterUpdate -notmatch "y|n")
{
    $compileAfterUpdate = Read-Host -Prompt 'Do you want to compile front and admin ? (Y/N)'
    if($compileAfterUpdate.ToLower() -eq 'n')
    {
        Exit
    }
}

Write-Output "Compiling Front ..."

cd "$frontFolder"
cmd.exe /c yarn
cmd.exe /c yarn setFiles
cmd.exe /c yarn release

Write-Output "Front compiled"

cd "$basePath"

Write-Output "Compiling Admin ..."

cd "$adminFolder"
cmd.exe /c pnpm i
cmd.exe /c gulp init

Write-Output "Admin compiled"

cd "$basePath"

